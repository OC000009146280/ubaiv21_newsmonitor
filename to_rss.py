from pandas import read_csv
from feedgen.feed import FeedGenerator
import datetime
from flask import Flask, Response
import json
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def sendmail(data):
    # Set credentials and establish connection to server
    smtp_server = "smtp.gmail.com"  # for example
    smtp_port = 587  # for example
    smtp_username = "User"
    smtp_password = "Password"
    smtp_conn = smtplib.SMTP(smtp_server, smtp_port)
    smtp_conn.starttls()
    smtp_conn.login(smtp_username, smtp_password)

    # Open a Json with tags and the subscribed mail addresses
    with open("tags.json", "r") as file:
        subs = json.load(file)
    # Map the mail adresses to the Tags in the dataset
    data_sub = data[data["Tag"].notna()]
    data_sub["Mail"] = data_sub["Tag"].map(subs)
    data_sub = data_sub.explode("Mail")

    # Aggregate the articles so that every subscriber gets one mail with all relevant articles
    for mail in data_sub.Mail.unique():
        to_mail = str(mail)
        body = "Hello, there is a new NewsBrew for you!\n\n"
        for index, entry in data_sub.loc[data_sub["Mail"] == mail].iterrows():
            sstr = f' {entry["Titel"]}\n {entry["Tag"]}\n {entry["Summary"]}\n Link: {entry["Link"]}\n\n'
            string += sstr

        # Create the mail and send it
        msg = MIMEMultipart()
        msg["From"] = smtp_username
        msg["To"] = to_mail
        msg["Subject"] = "Your weekly NewsBrew"
        msg.attach(MIMEText(str(body), "plain"))
        smtp_conn.sendmail(smtp_username, to_mail, msg.as_string())

    smtp_conn.quit()


app = Flask(__name__)


@app.route("/rss")
def rss():
    return Response(fg.rss_str(pretty=True), mimetype="text/xml")


###################################################################
###################################################################

if __name__ == "__main__":
    now = datetime.datetime.now(datetime.timezone.utc)

    data = read_csv("dataset_news_monitor.csv", parse_dates=True)

    fg = FeedGenerator()

    fg.title("UBA News Monitor")
    fg.link(href="http://ubafbiv.de/newsmonitor/rss/", rel="self", replace=True)
    fg.description("This is the News Monitor RSS Feed of FB IV of the Umweltbundesamt")

    for index, row in data.iterrows():
        row = row.apply(str)
        fe = fg.add_entry()
        fe.id(row["Link"])
        fe.title(row["Title"])
        fe.description(row["Summary"])
        fe.pubDate(now)  # da jeder unserer Input RSS Feeds ein eigenes DT Format nutzt eine Annäherung
        # fe.author("")

#    sendmail(data)
    app.run()
# fg.rss_file("uba_feed.xml")
