import pandas as pd
import numpy as np
import datetime as dt
import json

# load review export
output_to_rss_directly = pd.read_excel(r"C:/Users/dennis.gehri/BearingPoint GmbH/UBA Newsmonitor - General/testfiles/output_to_rss_directly_updated.xlsx")

#load config
with open("news_monitor_workflow\configfiles\config.json") as f:
    config = json.load(f)

# days to cutoff
cutoff_days = config["cleaning_output"]
cutoff_days = int(cutoff_days.split(sep="T")[0])

date_today = dt.datetime.now()

cutoff_date = date_today - dt.timedelta(days = cutoff_days)

#output_to_rss_directly[]

dates = output_to_rss_directly["Published"]

dates_datetime = []
for date in dates:
    date = date.split(sep="+")[0]
    date = dt.datetime.strptime(date , "%Y-%m-%d %H:%M:%S")
    dates_datetime.append(date < cutoff_date)

output_to_rss_directly_newest = output_to_rss_directly[dates_datetime]

from news_monitor_workflow.exceltable import output_exceltable
output_exceltable(output_to_rss_directly_newest,'output_to_rss_directly_newest.xlsx')