import pandas as pd
import json
import numpy as np

#prepare direct output
#reading file after ML classification scoring
data_output_full = pd.read_csv("C:/Users/tobias.megger/OneDrive - BearingPoint GmbH/UBA/NewsMonitor/testfiles/data_output_full.csv",sep=";")

# step only relevant for testfile as numbers are noted in german format as string, i.e. "0,9"
for i in range(0,len(data_output_full["Score"])):
#    print(data_output_full.iloc[i,np.where(data_output_full.columns=="Score")[0][0]])
    data_output_full.iloc[i,np.where(data_output_full.columns=="Score")[0][0]] = \
        float(data_output_full.iloc[i,np.where(data_output_full.columns=="Score")[0][0]].replace(',','.'))

#read config file that gholds relevant ML score configuration
with open("news_monitor_workflow\configfiles\config.json") as f:
    config = json.load(f)

if config["reviewer_is_absent"] == "yes":
    relevancy_limits = config["relevancy_limits"]['relevant_limits_normal']
    low_limit = float(relevancy_limits["limit_low"])
    high_limit = float(relevancy_limits["limit_high"])
else:
    relevancy_limits = config["relevancy_limits"]['relevant_limits_absence']
    low_limit = float(relevancy_limits["limit_low"])
    high_limit = float(relevancy_limits["limit_high"])

# collect relevant by keyword
output_feed_ContRel = data_output_full[data_output_full["ContainsRelevant"]==True]

# collect relevant by ML score
output_feed_ByScore = data_output_full[data_output_full["Score"]>=high_limit]

#concat
output_to_rss_directly = pd.concat([output_feed_ContRel,output_feed_ByScore])

#drop duplicates (that are both relevant by keyword as well as score)
output_to_rss_directly = output_to_rss_directly.drop_duplicates(
    subset=[
        "Link",
    ],
    ignore_index=True,
)

# reduce to only necessary columns
output_to_rss_directly = output_to_rss_directly[["Titel", "Summary", "Link", "Published"]]

# generate output
from news_monitor_workflow.exceltable import output_exceltable
output_exceltable(output_to_rss_directly,"output_to_rss_directly3.xlsx")

# prepare review output
# kick out negative keywords
data_output_full = data_output_full[data_output_full["ContainsIrrelevant"]==False]

# kick out positive keywords as those go to direkt output
data_output_full = data_output_full[data_output_full["ContainsRelevant"]==False]

# reduce dataset to all below review limit
output_to_review_smaller = data_output_full[data_output_full["Score"]<high_limit]

# and higher than discard limit
output_to_review = output_to_review_smaller[output_to_review_smaller["Score"]>=low_limit]

# remove duplicates by link
output_to_review = output_to_review.drop_duplicates(
    subset=[
        "Link",
    ],
    ignore_index=True,
)

# generate output
output_to_review.to_csv("output_to_review.csv", index = False, sep=";")
output_exceltable(output_to_review,"output_to_review.xlsx")