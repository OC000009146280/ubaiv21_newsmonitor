import pandas as pd
import numpy as np
import datetime as dt

# load review export
data_reviewed = pd.read_csv(r"C:/Users/tobias.megger/BearingPoint GmbH/UBA Newsmonitor - General/testfiles/Test_Power_Automate.csv")


# reduce to relevant only
reviewed_relevant = data_reviewed[data_reviewed["Relevant"]=="relevant"]

# clean summary (that have <div> statements)
new_elements = []
for item in reviewed_relevant['Summary']:
    item = item.split(sep='">')[1].split(sep='</')[0]
    new_elements.append(item)
reviewed_relevant['Summary'] = new_elements


# read previous outfile
output_to_rss_directly = pd.read_excel(r"C:/Users/tobias.megger/BearingPoint GmbH/UBA Newsmonitor - General/testfiles/output_to_rss_directly3.xlsx")

# update output 
output_to_rss_directly_updated = pd.concat([reviewed_relevant,output_to_rss_directly],sort=False,ignore_index=True)

# remove duplicates by link
output_to_rss_directly_updated = output_to_rss_directly_updated.drop_duplicates(
    subset=[
        "Link",
    ],
    ignore_index=True,
)

# change date format and convert to string that excel can read properly
published_new = []
for item in output_to_rss_directly_updated["Published"]:
    item = pd.to_datetime(item)
    
    published_new.append(str(item))
output_to_rss_directly_updated["Published"] = published_new

from news_monitor_workflow.exceltable import output_exceltable
output_exceltable(output_to_rss_directly_updated,'output_to_rss_directly_updated.xlsx')

# relevant are beeing shifted to output, not relevant not needed --> sorting out
data_reviewed_updated = data_reviewed[ data_reviewed["Relevant"]!="relevant" ][data_reviewed["Relevant"]!="nicht relevant" ]

# clean summary (that have <div> statements)
new_elements = []
for item in data_reviewed_updated['Summary']:
    try:
        item = item.split(sep='">')[1].split(sep='</')[0]
    except:
        item = item
    new_elements.append(item)
data_reviewed_updated['Summary'] = new_elements

output_exceltable(data_reviewed_updated,'data_reviewed_updated.xlsx')

