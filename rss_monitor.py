import feedparser
import pandas as pd
from datetime import datetime

feed_urls = [
    "https://www.tagesschau.de/xml/rss2/",
    "https://www.spiegel.de/schlagzeilen/tops/index.rss",
    "https://www.welt.de/feeds/topnews.rss",
    "https://www.n-tv.de/rss",
    "https://rss.sueddeutsche.de/app/service/rss/alles/index.rss?output=rss",
    "https://www.deutschlandfunk.de/rss-angebot-102.html",
    "https://www.taz.de/!p4608;rss/",
    "http://www.mz-web.de/extern/rss/panorama.xml",
    "https://www.berliner-zeitung.de/rss-feeds-der-berliner-zeitung-li.25605",
    "https://www.berliner-zeitung.de/feed.xml",
    "https://www.lebensmittelwarnung.de/bvl-lmw-de/opensaga/feed/alle/alle_bundeslaender.rss",
    "https://www.lanuv.nrw.de/index.php?type=101",
    "https://www.lanuv.nrw.de/index.php?type=102",
    "https://www.rbb24.de/aktuell/index.xml/feed=rss.xml",
    #     "https://www.polizei.bayern.de/rss_aktuelle_mitteilungen.xml",
    #     "https://www.polizei.sachsen.de/de/presse_rss_all.xml",
]


def rss(feeds):
    articles = []

    for feed_url in feeds:
        feed = feedparser.parse(feed_url)
        publisher = feed_url.split(".")[1]
        for entry in feed.entries:
            try:
                title, summary, link, published = entry.title, entry.summary, entry.link, entry.published
            except AttributeError:
                title, summary, link, published = entry.title, entry.title_detail, entry.link, entry.published

            articles.append(
                {
                    "Title": title,
                    "Summary": summary,
                    "Link": link,
                    "Published": published,
                    "Publisher": publisher,
                    "WorkedOn": 0,
                }
            )

    return pd.DataFrame(articles)


df = rss(feed_urls)
data_full = pd.read_csv(r"dataset_news_monitor.csv", index_col=0)
data_full = pd.concat([data_full, df], axis=0)
data_full = data_full.drop_duplicates()
data_full["Published"] = data_full.apply(
    lambda x: str(datetime.today()) if pd.isna(x["Published"]) else x["Published"], axis=1
)
# data_full["Summary"] = data_full.apply(lambda x: x["Title"] if pd.isna(x["Summary"]) else x["Summary"], axis=1)
data_full.to_csv(r"dataset_news_monitor_test.csv", index=False)
