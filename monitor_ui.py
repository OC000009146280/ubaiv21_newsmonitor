import pandas as pd
import os
import json
from warnings import filterwarnings

filterwarnings("ignore")
# os.system('cls')


def overwrite_save(df_0, df_1, ind):
    df_0.loc[ind, "Relevant"] = 1
    df_0.loc[ind, "WorkedOn"] = 1
    print(
        f"\nverfügbare Tags: { ', '.join(list(subs.keys()))}\nBitte achten Sie auf genaue Eingabe. Falls mehrere Tags vergeben werden sollen, bitte im Format 'Tag1,Tag2'\nFalls kein Tag vergeben werden soll, lassen Sie die Eingabe leer, dann erscheint die nächste Meldung."
    )
    while True:
        tags = input("Bitte geben sie hier die Tags für diesen Artikel an: ")
        tags = tags.replace(" ", "").split(sep=(","))
        if all(elem in list(subs.keys()) for elem in tags):
            df_0.loc[ind, "Tag"] = str(tags)
            break
    save = pd.concat([df_0, df_1], axis=0).sort_index(ascending=False)
    save.to_csv(csv_title, index=False)


def Landing_Page():
    print("This is the UBA News Monitor Interface, if you want to quit press Ctrl+C\n\n")


def review_news():
    for index, row in data_0.iterrows():
        # print("\033[1m" + row["Title"] + "\033[0m" + "\n")
        print(row["Title"])
        # print(f'by {row["Publisher"]}')
        print(row["Summary"], "\n")
        print(row["Link"], "\n")
        while True:
            rel = input("Ist diese Meldung relevant? (0 = Nein, 1 = Ja) ")

            if rel == "1":
                overwrite_save(data_0, data_1, index)
                break
            elif rel == "0":
                data_0.loc[index, "WorkedOn"] = 0
                break
            else:
                print("Value must be either 0 or 1")
        print("\n\n")


if __name__ == "__main__":
    csv_title = "data_output.csv"
    data = pd.read_csv(csv_title, parse_dates=True)
    with open("tags.json", "r") as file:
        subs = json.load(file)

    data["Relevant"] = 0
    data_0 = data.loc[data["WorkedOn"] == 0]
    data_1 = data.loc[data["WorkedOn"] == 1]

    Landing_Page()
    review_news()

    data = data.loc[data["Relevant"] == 1]
    data.drop("Relevant", axis=1, inplace=True)
    data.to_csv(csv_title, index=False)
