import pandas as pd
#from assign_incident_06 import data_full
import itertools
import numpy as np
# data_full.to_csv("data_zwischenschritt_3.csv", index = False)
from exceltable import output_exceltable


def gen_relev_data_feed(data,outputpath):

    data_reduced = data[data["ContainsRelevant"]==True]
    data_output = data_reduced[["Titel","Summary","Link","Published","WorkedOn"]]
    data.to_csv(str(outputpath+"/data_output_full.csv"), index = False, sep=";")
    data_output.to_csv(str(outputpath+"/data_output_redu.csv"), index = False, sep=";")

    
    output_exceltable(data,str(outputpath+"/data_output_full.xlsx"))
    output_exceltable(data_output,str(outputpath+"/data_output_redu.xlsx"))

    return None
