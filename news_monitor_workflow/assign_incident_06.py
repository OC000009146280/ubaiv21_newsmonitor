import pandas as pd
import numpy as np
import spacy
from tag_relevant_05 import data_full
from sklearn.cluster import AgglomerativeClustering

# df = pd.read_csv(r"C:\News_Monitor_UBA\UBA_News_Monitor\dataset_news_monitor.csv")
nlp = spacy.load("de_core_news_md")
stop_words = spacy.lang.de.stop_words.STOP_WORDS


def group_same(df: pd.DataFrame, indices: list):
    sub = df.loc[indices, :]
    one = indices.pop(0)
    df = df.drop(indices, axis=0)
    # sequence item 0: expected str instance, float found -> because of NA links
    df.loc[[one], "Link"] = ", ".join(str(link) for link in sub.Link.astype(str))

    return df.reset_index(drop=True)


def preprocess_text(text):
    doc = nlp(text.lower())
    tokens = [token.text for token in doc if not token.is_stop and not token.is_punct and not token.is_space]
    return " ".join(tokens)


def aggregate_news(df):
    threshold = 0.98
    preprocessed_texts = [
        preprocess_text(str(title) + " " + str(summary)) for title, summary in zip(df["Title"], df["Summary"])
    ]
    vectors = [nlp(text).vector for text in preprocessed_texts]
    # nonzero_inds = [i for i, v in enumerate(vectors) if any(v)] # wenn euc dann raus
    # vectors = [vectors[i] for i in nonzero_inds] # wenn euc dann raus
    # df = df.iloc[nonzero_inds].reset_index(drop=True) # wenn euc dann raus

    # Wie verhindern wir, dass ein LANUV Artikel von 2015 über Öl im Rhein mit einem von 2023 zusammengefasst wird
    try:
        cluster = AgglomerativeClustering(
            metric="cosine", linkage="average", distance_threshold=1 - threshold, n_clusters=None
        )

        df["cluster"] = cluster.fit_predict(vectors)
    except ValueError:
        cluster = AgglomerativeClustering(
            metric="euclidean", linkage="average", distance_threshold=1 - threshold, n_clusters=None
        )

    df["cluster"] = cluster.fit_predict(vectors)

    for cluster in df.cluster.unique():
        inds = list(df[df["cluster"] == cluster].index)
        df = group_same(df, inds)
    return df


data_full = aggregate_news(data_full)
data_full.to_csv("data_zwischenschritt_3.csv", index=False, sep=";")
