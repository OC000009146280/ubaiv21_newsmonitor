import json

from load_feeds_fill_file_02 import update_df
from drop_irrelevant_03 import negative_keywords
from filter_04 import positive_keywords
from ML_relevant import ML_relevant
from tag_relevant_05 import mark_all_relevant, mark_all_irrelevant
from generate_output import gen_relev_data_feed



def main():

    # load configuration file
    with open("news_monitor_workflow\configfiles\config.json") as f:
        config = json.load(f)
    
    # get feeds from configuration file
    feed = config["RSS_feeds"]
    data_full = update_df(feed)

    #neg_keyw = config["negative_keywords"] 
    #data_full = negative_keywords(data_full,neg_keyw)

    #pos_keyw = config["positive_keywords"]
    #data_full = positive_keywords(data_full,pos_keyw)

    data_full = ML_relevant(data_full)
    
    #data_full = mark_all_relevant(data_full)
    #data_full = mark_all_irrelevant(data_full)
    
    #outputpath = config["output_path"]
    #gen_relev_data_feed(data_full,outputpath)
   
    return None



if __name__ == "__main__":
    main()