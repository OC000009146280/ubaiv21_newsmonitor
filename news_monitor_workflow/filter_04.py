import pandas as pd
#from drop_irrelevant_03 import data_drop, data_full
import json, re, pickle
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords

def positive_keywords(data, pos_keyw):

    for keyword in pos_keyw:
        data[str("contains_positive_Keyword (" + keyword + ")")] = data["Title_Summary"
        ].str.contains(keyword)

    return data

