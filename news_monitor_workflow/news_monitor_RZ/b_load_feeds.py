import pandas as pd
import feedparser
import datetime
import numpy as np


def update_df(feed):

    def news_article(entry):
        # if (entry.published):
        #     publ = datetime.datetime.now()
        # else:
        #     publ = entry.published
        return entry.title, entry.summary, entry.link, entry.published#,publ


    def police_report(entry):
        # if np.isnan(entry.published):
        #     publ = datetime.datetime.now()
        # else:
        #     publ = entry.published
        return entry.title, entry.title_detail, entry.link, entry.published#,publ


    def rss(feeds):
        articles = []

        for feed_url in feeds:
            feed = feedparser.parse(feed_url)
            for entry in feed.entries:
                try:
                    (
                        title,
                        summary,
                        link,
                        published,
                    ) = news_article(entry)

                except:
                    title, summary, link, published = police_report(entry)
                    continue

                articles.append({"Titel": title, "Summary": summary, "Link": link, "Published": published, "WorkedOn": 0})

        return pd.DataFrame(articles)


    df = rss(feed)
    df["Title_Summary"] = df["Titel"] + " " + df["Summary"]

    # clean
    df.drop(df[df["Title_Summary"].str.contains("{'type'")].index, axis=0, inplace=True)
    df.drop(df[df["Title_Summary"].str.contains("<img")].index, axis=0, inplace=True)

    try:
        data_full = pd.read_csv("dataset_news_monitor.csv",sep=";")
    except:
        df.to_csv("dataset_news_monitor.csv", index = False, sep=";")
        data_full = pd.read_csv("dataset_news_monitor.csv",sep=";")


    data_full = pd.concat([data_full,df])

    data_full.to_csv("dataset_news_monitor.csv", index=False,sep=";")
    # data_full["Published_enhanced"] = data_full.apply(
    #     lambda x: datetime.datetime.now() if pd.isna(x["Published"]) else x["Published"], axis=1
    # )
    # data_full["Published_converted"] = pd.to_datetime(data_full["Published"])
    data_full = data_full.drop_duplicates(
        subset=[
            "Link",
        ],
        ignore_index=True,
    )
    data_full.dropna(axis=0, subset=["Titel", "Summary"])

    # Filter für aktuelle LANUV artikel goes here
    return data_full

    # #clean
    # data_full.drop(data_full[data_full["Title_Summary"].str.contains("{'type'")].index, axis=0, inplace=True)
    # data_full.drop(data_full[data_full["Title_Summary"].str.contains("<img")].index, axis=0, inplace=True)

    # data_full.to_csv(r"dataset_news_monitor.csv", index=False, sep=";")
    # data_full.to_csv(r"dataset_news_monitor_raw.csv", index=False, sep=";")
