import json

from b_load_feeds import update_df
from c_ML_relevant import ML_relevant



def main():

    # load configuration file
    with open("config.json") as f:
        config = json.load(f)
    
    # get feeds from configuration file
    feed = config["RSS_feeds"]
    data_full = update_df(feed)

    # get relevant feeds
    data_full = ML_relevant(data_full)
   
    return None



if __name__ == "__main__":
    main()