import pandas as pd
import json, re, pickle
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords

def ML_relevant(data):

    with open('model.pkl', 'rb') as f:
        clf = pickle.load(f)

    with open('vectorizer.pkl', 'rb') as f:
        vectorizer = pickle.load(f)

    def remove_tags(text):
        if text == text:
            text = re.sub(r"(<.*?>|_x000D_|\n)", " ", text)
            return text.strip()


    def preprocess(df):
        # df=df.dropna(subset=["Title","Summary", "Title_Summary"])
        df["Title_Summary"] = df["Title_Summary"].apply(remove_tags)
        # vectorizer = CountVectorizer(stop_words=stopwords.words("german"))
        mat = vectorizer.transform(df["Title_Summary"])
        return df, mat


    def predict(df):
        data, matrix = preprocess(df)
        data["Relevant_ML"] = clf.predict(matrix)
        return data[data["Relevant_ML"] == 1]
    
    data = predict(data)
    data.to_excel("data_predict.xlsx", index = False)
    return data

