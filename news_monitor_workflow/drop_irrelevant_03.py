import pandas as pd
import numpy as np
import json
# Deprecated und zu löschen
# ywords from json

def negative_keywords(data, neg_keyw):

    # drop from list
    for keyword in neg_keyw:
        data[str("contains_negative_Keyword (" + keyword + ")")] = data["Title_Summary"
        ].str.contains(keyword)

    return data