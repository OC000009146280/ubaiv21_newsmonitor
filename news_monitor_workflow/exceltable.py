import pandas as pd

def output_exceltable(input_dataframe,output_filename):

    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(output_filename, engine='xlsxwriter')

    # Write the dataframe data to XlsxWriter. 
    input_dataframe.to_excel(writer, sheet_name='Sheet1', startrow=1, header=False, index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']

    # Get the dimensions of the dataframe.
    (max_row, max_col) = input_dataframe.shape

    # Create a list of column headers, to use in add_table().
    column_settings = [{'header': column} for column in input_dataframe.columns]

    # Add the Excel table structure. Pandas will add the data.
    worksheet.add_table(0, 0, max_row, max_col - 1, {'columns': column_settings})

    # Close the Pandas Excel writer and output the Excel file.
    writer.close()

    return None
