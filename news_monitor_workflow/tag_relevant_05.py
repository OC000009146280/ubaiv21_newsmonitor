import pandas as pd
#from filter_04 import data_full
import itertools
import numpy as np

def mark_all_relevant(data):
    # mark as relevant when contains positive keyword
    positive_index = []
    for pos_col in data.loc[:, ["contains_positive_Keyword" in i for i in data.columns]].columns:
        positive_index = list(itertools.chain(positive_index, np.where(data[pos_col])[0]))

    data["ContainsRelevant"] = False
    data.loc[positive_index, "ContainsRelevant"] = True



#        data_full.to_csv("data_zwischenschritt_2.csv", index=False, sep=";")
    return data

def mark_all_irrelevant(data):
    # mark as relevant when contains positive keyword
    negative_index = []
    for neg_col in data.loc[:, ["contains_negative_Keyword" in i for i in data.columns]].columns:
        negative_index = list(itertools.chain(negative_index, np.where(data[neg_col])[0]))

    data["ContainsIrrelevant"] = False
    data.loc[negative_index, "ContainsIrrelevant"] = True


#        data_full.to_csv("data_zwischenschritt_2.csv", index=False, sep=";")
    return data